package main

import (
	"fmt"
	"gin-vue/core"
	"gin-vue/global"
	"gin-vue/routers"
)

func main() {
	//读取配置文件
	core.InitConf()
	//连接数据库
	global.DB = core.InitGorm()
	//
	router := routers.InitRouter()
	addr := global.Config.System.Addr()
	fmt.Printf("gvb server运行在：%s", addr)
	router.Run(addr)
}
