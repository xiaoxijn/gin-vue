package routers

import (
	"gin-vue/api"
	"github.com/gin-gonic/gin"
)

func SettingsRouter(router *gin.Engine) {
	settingsApi := api.ApiGroupApp.SettingsApi
	router.GET("/setting", settingsApi.SettingsInfoView)
}
