package routers

import (
	"gin-vue/api"
	"github.com/gin-gonic/gin"
)

func UsersRouter(router *gin.Engine) {
	userApi := api.ApiGroupApp.UserApi
	router.GET("/count", userApi.UserCount)
	router.POST("/login", userApi.UserLogin)
	router.GET("/page", userApi.UserPage)
	router.POST("/user/save", userApi.UserSave)
}
