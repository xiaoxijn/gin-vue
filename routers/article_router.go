package routers

import (
	"gin-vue/api"
	"github.com/gin-gonic/gin"
)

func ArticleRouter(router *gin.Engine) {
	articleApi := api.ApiGroupApp.ArticlesApi
	router.GET("/article/count", articleApi.ArticleCount)
	router.GET("/article/type", articleApi.ArticleType)
	router.GET("/article/page", articleApi.Articles)
	router.GET("/article/list", articleApi.Article)
}
