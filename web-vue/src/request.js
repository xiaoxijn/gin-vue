import axios from 'axios'
import ElementUI from "element-ui";

const request = axios.create({
    baseURL: 'http://10.3.25.80:8090',
    timeout: 5000
})

// request 拦截器
// 可以自请求发送前对请求做一些处理
// 比如统一加token，对请求参数统一加密
request.interceptors.request.use(config => {
    config.headers['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    return config
}, error => {
    return Promise.reject(error)
});



export default request

