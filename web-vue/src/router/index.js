import Vue from 'vue'
import VueRouter from 'vue-router'
import store from "@/store";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: '404',
    component: () => import("../views/404")
  },
  {
    path: '/login',
    name: 'login',
    component: () => import("../views/login.vue")
  },
  {
    path: '/main',
    name: 'main',
    component: () => import("../views/main"),
    redirect: "/home",
    children: [
      {
        path: '/home',
        name: '首页',
        component: () => import("../views/Home.vue")
      },
      {
        path: '/user',
        name: '用户管理',
        component: () => import("../views/User.vue")
      },
      {
        path: '/article',
        name: '文章管理',
        component: () => import("../views/Article.vue")
      },
      {
        path: '/menu',
        name: '菜单管理',
        component: () => import("../views/Menu.vue")
      },
    ]
  },

]

const router = new VueRouter({
  mode: 'history',
  routes
})

// 路由守卫
router.beforeEach((to, from, next) => {
  localStorage.setItem("currentPathName", to.name)  // 设置当前的路由名称，为了在Header组件中去使用
  store.commit("setPath")  // 触发store的数据更新
  next()  // 放行路由
})

export default router
