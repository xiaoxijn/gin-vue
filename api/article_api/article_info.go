package article_api

import (
	"gin-vue/global"
	"gin-vue/modles/article"
	"gin-vue/modles/res"
	"github.com/gin-gonic/gin"
	"strconv"
)

// ArticleCount 文章总数
func (ArticlesApi) ArticleCount(c *gin.Context) {
	var article []article.Article
	rows := global.DB.Find(&article).RowsAffected
	res.OkWithData(rows, c)
}

func (ArticlesApi) Article(c *gin.Context) {
	var article []article.Article
	global.DB.Find(&article)
	res.OkWithData(article, c)
}

// Articles 文章列表
func (ArticlesApi) Articles(c *gin.Context) {
	//分页查询通用写法
	var article []article.Article
	// 一页多少条
	limit, _ := strconv.Atoi(c.Query("pageSize"))
	//fmt.Println("一页：", limit)
	// 第几页
	page, _ := strconv.Atoi(c.Query("pageNum"))
	name := c.Query("name")
	if limit == 0 || page == 0 {
		res.FailWithMsg("查询有误", c)
		return
	}
	offset := (page - 1) * limit //从第几条数据（offset+1）开始
	if name == "" {
		global.DB.Limit(limit).Offset(offset).Find(&article)
	}
	//fmt.Println("第：", page)
	global.DB.Limit(limit).Offset(offset).Find(&article)
	res.OkWithData(article, c)
}

// ArticleType 文章总数
func (ArticlesApi) ArticleType(c *gin.Context) {
	var articleType []article.ArticleType
	global.DB.Find(&articleType)
	res.OkWithData(articleType, c)
}
