package api

import (
	"gin-vue/api/article_api"
	"gin-vue/api/settings_api"
	"gin-vue/api/user_api"
)

type ApiGroup struct {
	SettingsApi settings_api.SettingsApi //系统设置
	UserApi     user_api.UsersApi        //用户管理
	ArticlesApi article_api.ArticlesApi  //文章管理
}

var ApiGroupApp = new(ApiGroup)
