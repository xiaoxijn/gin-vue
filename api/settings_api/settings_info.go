package settings_api

import (
	"gin-vue/modles/res"
	"github.com/gin-gonic/gin"
)

func (SettingsApi) SettingsInfoView(c *gin.Context) {
	res.Ok(map[string]string{}, "hyc", c)
}
