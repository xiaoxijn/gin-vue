package user

import (
	"time"
)

// User 用户表
type User struct {
	ID         int        `json:"id"`
	Name       string     `json:"name"`
	No         string     `json:"no"`
	Password   string     `json:"password"`
	Age        int        `json:"age"`
	Sex        int        `json:"sex"`
	Email      string     `json:"email"`
	CreateTime *time.Time `json:"createTime"`
}

//func (user *User) GetUser() (users *User, err error) {
//	global.DB.Find(&users)
//	return users, nil
//}
