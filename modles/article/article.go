package article

import "time"

// Article 文章列表
type Article struct {
	ID         int        `json:"id"`
	Name       string     `json:"name"`
	Type       string     `json:"type"`
	UserId     int        `json:"userId"`
	CreateTime *time.Time `json:"createTime"`
}

// ArticleType 文章类别
type ArticleType struct {
	ID    int    `json:"id"`
	Name  string `json:"name"`
	Value int    `json:"value"`
}
