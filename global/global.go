package global

import (
	"gin-vue/config"
	"gorm.io/gorm"
)

//全局变量
var (
	Config *config.Config
	DB     *gorm.DB
)
