package core

import (
	"fmt"
	"gin-vue/global"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
	"time"
)

// InitGorm 初始化gorm
func InitGorm() *gorm.DB {
	if global.Config.Mysql.Host == "" {
		fmt.Errorf("未配置mysql，取消gorm连接")
		return nil
	}
	dsn := global.Config.Mysql.Dsn()
	var mysqlLogger logger.Interface
	if global.Config.System.Env == "debug" { //判断环境
		//开发环境显示所有的sq1
		mysqlLogger = logger.Default.LogMode(logger.Info)
	} else {
		mysqlLogger = logger.Default.LogMode(logger.Error) // 只打印错误的sql
	}
	//global.MysqlLog = logger .Default .LogMode(logger.Info)
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{
		Logger: mysqlLogger,
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true, // 单数表名
		},
	})
	if err != nil {
		fmt.Errorf("MySql连接诶失败。。。。")
	}
	sqlDB, _ := db.DB()
	sqlDB.SetMaxIdleConns(10)
	// 最大空闲连接数
	sqlDB.SetMaxOpenConns(100)
	// 最多可容纳
	sqlDB.SetConnMaxLifetime(time.Hour * 4) // 连接最大复用时间，不能超过mysql的wait_timeout
	return db
}
